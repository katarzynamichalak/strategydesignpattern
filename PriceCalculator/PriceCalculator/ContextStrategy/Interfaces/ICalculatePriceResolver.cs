﻿using PriceCalculator.StrategyInterface;

namespace PriceCalculator.ContextStrategy.Interfaces
{
    public interface ICalculatePriceResolver
    {
        ICalculatePrice Resolve(PriceType priceType);
    }
}
