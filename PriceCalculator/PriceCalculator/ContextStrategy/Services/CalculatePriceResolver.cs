﻿using System;
using System.Collections.Generic;
using System.Linq;
using PriceCalculator.StrategyInterface;
using PriceCalculator.ContextStrategy.Interfaces;

namespace PriceCalculator.ContextStrategy.Services
{
    public class CalculatePriceResolver : ICalculatePriceResolver
    {
        private readonly IEnumerable<ICalculatePrice> calculatePriceMethods;

        public CalculatePriceResolver(IEnumerable<ICalculatePrice> calculatePriceMethods)
        {
            this.calculatePriceMethods = calculatePriceMethods;
        }
        public ICalculatePrice Resolve(PriceType priceType)
        {
            ICalculatePrice calculatePriceMethod =
                calculatePriceMethods.FirstOrDefault(x => x.PriceType == priceType);

            if (calculatePriceMethod == null)
            {
                throw new NotImplementedException("There is no price calculation method");
            }

            return calculatePriceMethod;
        }
    }
}
