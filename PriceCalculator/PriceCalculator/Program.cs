﻿using System;
using Autofac;
using PriceCalculator.Strategies;
using PriceCalculator.StrategyInterface;
using PriceCalculator.ContextStrategy.Services;
using PriceCalculator.ContextStrategy.Interfaces;

namespace PriceCalculator
{
    public enum PriceType
    {
        Low = 1,
        Normal = 2,
        Overprice = 3
    }
    public class Program
    {  
        static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<LowPrice>().As<ICalculatePrice>();
            builder.RegisterType<NormalPrice>().As<ICalculatePrice>();
            builder.RegisterType<OverpricePrice>().As<ICalculatePrice>();
            builder.RegisterType<CalculatePriceResolver>().As<ICalculatePriceResolver>();  
            var container = builder.Build();

            decimal result = 0;
            while (true)
            {
                using (var scope = container.BeginLifetimeScope())
                {
                    int userInput = DisplayMenu();
                    var service = container.Resolve<ICalculatePriceResolver>();
                    result = service.Resolve((PriceType)userInput).CalculatePrice(100);
                    ShowResult(result);
                }
            }
        }

        static public int DisplayMenu()
        {
            Console.WriteLine("Choose price");
            Console.WriteLine();
            Console.WriteLine("1. Low price");
            Console.WriteLine("2. Normal price");
            Console.WriteLine("3. Overprice proce");
            var result = Console.ReadLine();
            return Convert.ToInt32(result);
        }

        static public void ShowResult(decimal result)
        {
            Console.WriteLine("Result: {0}", result);
        }
    }  
}
