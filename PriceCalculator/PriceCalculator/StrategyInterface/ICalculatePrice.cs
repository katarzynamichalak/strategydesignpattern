﻿
namespace PriceCalculator.StrategyInterface
{
    public interface ICalculatePrice
    {
        PriceType PriceType { get; }
        decimal CalculatePrice(decimal price);
    }
}
