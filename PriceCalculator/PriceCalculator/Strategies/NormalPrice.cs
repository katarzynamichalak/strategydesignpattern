﻿using PriceCalculator.StrategyInterface;

namespace PriceCalculator.Strategies
{
    public class NormalPrice : ICalculatePrice
    {
        public PriceType PriceType { get; private set; }

        public NormalPrice()
        {
            PriceType = PriceType.Normal;
        }
        public decimal CalculatePrice(decimal price)
        {
            return price;
        }
    }
}
