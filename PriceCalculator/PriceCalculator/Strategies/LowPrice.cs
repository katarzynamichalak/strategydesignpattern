﻿using PriceCalculator.StrategyInterface;

namespace PriceCalculator.Strategies
{
    public class LowPrice : ICalculatePrice
    {
        public PriceType PriceType { get; private set; }

        public LowPrice()
        {
            PriceType = PriceType.Low;
        }
        public decimal CalculatePrice(decimal price)
        {
            var result = decimal.Multiply(price, 0.5m);
            return result;
        }
    }
}
