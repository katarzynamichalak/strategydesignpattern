﻿using PriceCalculator.StrategyInterface;

namespace PriceCalculator.Strategies
{
    public class OverpricePrice : ICalculatePrice
    {
        public PriceType PriceType { get; private set; }

        public OverpricePrice()
        {
            PriceType = PriceType.Overprice;
        }
        public decimal CalculatePrice(decimal price)
        {
            var result = decimal.Multiply(price, 1.5m);
            return result;
        }
    }
}
